# jacoco2cobertura

Docker image to allow java projects that use jacoco to use the new codecoverage feature of gitlab.

The image includes 2 scripts.
* cover2cover.py (forked from https://github.com/rix0rrr/cover2cover ; includes open PRs in that repo)
  * Converts jacoco xml reports to cobertura xml reports
* sourc2filename.py
  * reads the `<source></source>` tag and prepends the path to the filename attribute of each class.  
    This is necessary, because gitlab currently ignores that tag and expects the filename attribute to include the full path from the project root.  
    

# Usage:

```yaml
stages:
  - build
  - test
  - visualize
  - deploy

test-jdk11:
  stage: test
  image: maven:3.6.3-jdk-11
  script:
    - 'mvn $MAVEN_CLI_OPTS clean org.jacoco:jacoco-maven-plugin:prepare-agent test jacoco:report'
  artifacts:
    paths:
      - target/site/jacoco/jacoco.xml

coverage-jdk11:
  stage: visualize
  image: haynes/jacoco2cobertura:1.0.3
  script:
    - 'python /opt/cover2cover.py target/site/jacoco/jacoco.xml src/main/java > target/site/coverage.xml'
    - 'python /opt/source2filename.py target/site/coverage.xml'
  needs: ["test-jdk11"]
  dependencies:
    - test-jdk11
  artifacts:
    reports:
      cobertura: target/site/coverage.xml
```

# Known Limitations:

Only works if there is only one `<source></source>` tag in the coverage.xml
